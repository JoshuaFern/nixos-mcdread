# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

#
# General configuration for my OS
#

{ config, pkgs, ... }:

{
  nix.package = pkgs.nixUnstable; # this is required until nix 2.4 is released
  nix.extraOptions = ''
    experimental-features = nix-command flakes
    '';
  nix.trustedUsers = [ "root" "@wheel" ];

  boot.initrd.postDeviceCommands = ''
    echo bfq > /sys/block/sda/queue/scheduler # Use bfq i/o scheduler
  '';

  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot.consoleMode = "max"; # Console resolution
    systemd-boot.enable = true;
    efi.canTouchEfiVariables = true;
    timeout = 1; # Set timeout to 1 for faster boot speeds.
  };

  boot.kernelModules = [ "tcp_bbr" "bfq" ];
  boot.kernel.sysctl = {
    "net.core.default_qdisc" = "fq";
    "net.ipv4.tcp_congestion_control" = "bbr";
    "net.ipv4.tcp_slow_start_after_idle" = "0"; # Should help to improve performance in some cases.
    "net.ipv4.conf.all.accept_redirects" = "0"; # Avoid DNS poisoning attack
    "net.ipv4.conf.default.accept_redirects" = "0"; # Avoid DNS poisoning attack
  };

  boot.kernelParams = [
    "vga=0x034d" # 1080p 24bit framebuffer
  ];

  boot.tmpOnTmpfs = true; # /tmp using the tmpfs filesystem which stores files in RAM

  # Set your time zone.
  time.timeZone = "Europe/Amsterdam";

  networking = {
    networkmanager.enable = true;
    nameservers = [ "208.67.222.222" ];  #"9.9.9.9" "94.140.14.14"
    defaultGateway.address = "192.168.1.1"; #Router address
    resolvconf.dnsExtensionMechanism = false; #Random recommend worked for laptop slow internet
    useDHCP = false; #deprecated, therefore set to false
  };

  # rtkit is optional but recommended for pipewire
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "sv-latin1";
  };

  programs.zsh = {
    enable = true;
    ohMyZsh = {
      enable = true;
    };
  };

  services.disnix.enableMultiUser = false;
  environment = {
    variables = { EDITOR = "nvim"; };
    homeBinInPath = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users = {
    defaultUserShell = pkgs.zsh;
    users.mcdread = {
      isNormalUser = true;
      initialPassword = "swordfish";
      extraGroups = [ "audio" "networkmanager" "wheel" ];
    };
  };

  programs.git = {
    enable = true;
  };

  #virtualisation.docker.enable = true;

  #hardware.steam-hardware.enable = true; # Sets udev rules for Steam Controller, among other devices

  xdg.portal.enable = true;
  xdg.portal.extraPortals = with pkgs; [ xdg-desktop-portal-gtk ];
  xdg.portal.gtkUsePortal = true;
  services.flatpak.enable = true;
  services.earlyoom.enable = true; # Enable early out of memory killing.
  services.earlyoom.freeMemThreshold = 3;
  services.fstrim.enable = true; # Management service for SSDs

  networking.firewall.allowedTCPPorts = [
    27036 # Steam Remote Play
    27037 # Steam Remote Play
  ];
  networking.firewall.allowedUDPPorts = [
    27031 # Steam Remote Play
    27036 # Steam Remote Play
  ];

    networking.hosts."0.0.0.0" = [ # Block ads / tracking
      # Firefox
      "location.services.mozilla.com"
      "shavar.services.mozilla.com"
      "incoming.telemetry.mozilla.org"
      "ocsp.sca1b.amazontrust.com"
      # GameAnalytics
      "api.gameanalytics.com"
      "rubick.gameanalytics.com"
      # Google
      "www.google-analytics.com"
      "ssl.google-analytics.com"
      "www.googletagmanager.com"
      "www.googletagservices.com"
      # Redshell
      "api.redshell.io"
      "treasuredata.com"
      "api.treasuredata.com"
      "in.treasuredata.com"
      # Spotify
      "apresolve.spotify.com"
      "heads4-ak.spotify.com.edgesuite.net"
      "redirector.gvt1.com"
      # Unity Engine
      "unityads.unity3d.com.edgekey.net"
      "cdn-store-icons-akamai-prd.unityads.unity3d.com.edgekey.net"
      "cdp.cloud.unity3d.com"
      "perf-events.cloud.unity3d.com"
      "api.uca.cloud.unity3d.com"
      "config.uca.cloud.unity3d.com"
      "data-optout-service.uca.cloud.unity3d.com"
      "userreporting.cloud.unity3d.com"
      "ads.prd.ie.internal.unity3d.com"
      "ads-game-configuration-master.ads.prd.ie.internal.unity3d.com"
      "publisher-event.ads.prd.ie.internal.unity3d.com"
      "ads-privacy-api.prd.mz.internal.unity3d.com"
      "tracking.prd.mz.internal.unity3d.com"
      "stats.unity3d.com"
      "unityads.unity3d.com"
      "ads-brand-postback.unityads.unity3d.com"
      "ads-game-configuration.unityads.unity3d.com"
      "adserver.unityads.unity3d.com"
      "adsx.unityads.unity3d.com"
      "nginx-auction-prd-gcp.adsx.unityads.unity3d.com"
      "admediator.unityads.unity3d.com"
      "auction.unityads.unity3d.com"
      "auction-load.unityads.unity3d.com"
      "auiopt.unityads.unity3d.com"
      "cdn.unityads.unity3d.com"
      "cdn-creatives-highwinds-prd.unityads.unity3d.com"
      "cdn-webview-pge.unityads.unity3d.com"
      "config.unityads.unity3d.com"
      "dsp-tracking.unityads.unity3d.com"
      "dsp-vast.unityads.unity3d.com"
      "geocdn.unityads.unity3d.com"
      "httpkafka.unityads.unity3d.com"
      "pge.unityads.unity3d.com"
      "publisher-config.unityads.unity3d.com"
      "publisher-event.unityads.unity3d.com"
      "thind.unityads.unity3d.com"
      "webview.unityads.unity3d.com"
      # Unreal Engine 4
      "tracking.epicgames.com"
      "tracking.unrealengine.com"
    ];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?

  systemd.extraConfig = "DefaultLimitNOFILE=1048576"; # Set limits for Esync: https://github.com/lutris/docs/blob/master/HowToEsync.md

}

