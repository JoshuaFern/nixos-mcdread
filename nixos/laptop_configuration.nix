# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

#
# Config for laptop with Nvidia Optimus (hybrid) gpu. 
# Not currently setup for gaming (except remote play ofc)
#

{ config, pkgs, ... }:

{
  networking = {
    hostName = "nixos-laptop"; # Flake rebuild uses this value
    interfaces.enp9s0.useDHCP = true; # "ip link show" to get a list of interfaces
    interfaces.wlp8s0.useDHCP = true;
  };

  # Enable the X11 windowing system. Transition this to home-manager
  services.xserver = {
    enable = true;
    layout = "se";
    xkbOptions = "eurosign:e";
    displayManager.startx.enable = true;
    windowManager.i3 = {
      enable = true;
      extraSessionCommands = "PATH=$PATH:~/bin/";
      extraPackages = with pkgs; [
        feh
        dmenu #application launcher most people use
        i3status
        xdotool
        ];
    };
  };

}


