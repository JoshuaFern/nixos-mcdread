# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

#
# Settings for desktop pc with AMD gpu
#

{ config, pkgs, ... }:

{
  networking = {
    hostName = "nixos-desktop"; # Flake rebuild uses this value
    interfaces.enp6s0.useDHCP = true; # "ip link show" to get a list of interfaces
  };

  # Explicitly set some things for gaming with amd gpu
  boot.initrd.kernelModules = [ "amdgpu" ];
  hardware.cpu.intel.updateMicrocode = true;
  hardware.opengl = {
    driSupport = true;
    driSupport32Bit = true;
    extraPackages = with pkgs; [
      rocm-opencl-icd
      rocm-opencl-runtime
      amdvlk
    ];
    extraPackages32 = with pkgs; [
      driversi686Linux.amdvlk    
    ];
  };

  #hardware.steam-hardware.enable = true; # Sets udev rules for Steam Controller, among other devices

  powerManagement.enable = false; # Desktops don't need powermanagement

  programs.xwayland.enable = true; # Will probably be requred forever

  # Keep in mind that you want to eventually transition to having home-manager manage sway,
  # Having it configured like this means that home-manager is limited in what it can manage.
  programs.sway = { 
    enable = true;
    extraSessionCommands = ''
      export PATH=$PATH:~/bin/
      # SDL:
      export SDL_VIDEODRIVER=wayland
      # QT (needs qt5.qtwayland in systemPackages):
      export QT_QPA_PLATFORM=wayland-egl
      export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"
      # Fix for some Java AWT applications (e.g. Android Studio),
      # use this if they aren't displayed properly:
      export _JAVA_AWT_WM_NONREPARENTING=1
    '';
    wrapperFeatures.gtk = true;
    extraPackages = with pkgs; [
      dmenu-wayland
      imv
      swaybg
      wl-clipboard
      #xwayland # use programs.xwayland.enable = true; instead
      qt5.qtwayland
    ];
  };


}

