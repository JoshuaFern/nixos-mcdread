# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{

  # List packages installed in system profile (TODO transition many of these to home-manager). 
  # To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    bitwarden
    bitwarden-cli
    chatterino2
    curl
    easyeffects #audio compressor for factorio
    file
    firefox
    foot
    ffmpeg
    freetube
    ghc
    htop
    kakoune #replacement for vim? :o
    kitty
    legendary-gl
    links2 #replacement for Lynx?
    lynx
    mpv
    ncpamixer #ncurses pavucontrol

    (neovim.override {
      vimAlias = true;
      configure = {
        packages.myPlugins = with pkgs.vimPlugins; {
          start = [ vim-lastplace vim-nix vim-hexokinase ]; 
          opt = [];
        };
        customRC = ''
          " your custom vimrc
          set nocompatible
          set backspace=indent,eol,start
          set number relativenumber ruler
          set hlsearch
          set tabstop=3 shiftwidth=3 expandtab
          set clipboard=unnamedplus
          set termguicolors
          let g:Hexokinase_highlighters = [ 'backgroundfull' ]
          " 
        '';
      };
    })

    (wrapOBS {
      plugins = with obs-studio-plugins; [
        wlrobs
      ];
    })

    nyxt
    pavucontrol
    pscircle
    python3
    qutebrowser
    streamlink
    streamlink-twitch-gui-bin
    sqlitebrowser
    tree
    wget
    ytcc
    yt-dlp
    xterm
    xdg-utils
    zathura #pdf viewer
  ];

}

