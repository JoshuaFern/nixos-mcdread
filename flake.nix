{
  description = "My personal Nixos configuration flake.";
  
  nixConfig = {
    extra-experimental-features = "nix-command flakes ca-reference";
    extra-substituters = "https://nix-community.cachix.org";
    extra-trusted-public-keys = "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=";
  };
  
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    #nur.url = "github:nix-community/NUR"
  };

  outputs = { self, nixpkgs, home-manager, nur, ... } @inputs: 
  #let overlays = [ nur.overlay ];
  #in 
  {
    nixosConfigurations = {
      nixos-desktop = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [ 
          {
            #nixpkgs.overlays = overlays;
            nixpkgs.config.allowUnfree = false;
          }
          (import ./hardware-configuration.nix)
          (import ./nixos/configuration.nix)
          (import ./nixos/packages.nix)
          (import ./nixos/desktop_configuration.nix)
          #(import ./udev.nix)

          home-manager.nixosModules.home-manager
          {
            networking.hostName = "nixos-desktop";
            home-manager.extraSpecialArgs = { inherit inputs; };
          }
        ];
      };

      nixos-laptop = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules = [ 
          {
            #nixpkgs.overlays = overlays;
            nixpkgs.config.allowUnfree = false;
          }
          (import ./hardware-configuration.nix)
          (import ./nixos/configuration.nix)
          (import ./nixos/packages.nix)
          (import ./nixos/laptop_configuration.nix)
          #(import ./udev.nix)

          home-manager.nixosModules.home-manager
          {
            networking.hostName = "nixos-laptop";
            home-manager.extraSpecialArgs = { inherit inputs; };
          }
        ];
      };
    };
  };
}
