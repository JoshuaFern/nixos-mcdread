#!/usr/bin/env sh
SCRIPT_PATH=$(dirname "$0")
warn=$(tput bold && tput setaf 0 && tput setab 7)
normal=$(tput sgr0)

echo "${warn}REBUILD IMMINENT:${normal} Are you sure you want to?"
read -n1 -rsp $'Press any key to continue or Ctrl+C to exit...\n'

# Generate "hardware-configuration.nix" with: nixos-generate-config
echo "         sudo cp /etc/nixos/hardware-configuration.nix SCRIPT_PATH"
sudo cp "/etc/nixos/hardware-configuration.nix" "${SCRIPT_PATH}"
sudo chown $USER:users hardware-configuration.nix
echo "         git add ."
git add .

# Important to note that the flake rebuilds based on hostname
echo "         sudo nixos-rebuild switch --flake SCRIPT_PATH#hostname"
sudo nixos-rebuild switch --flake "${SCRIPT_PATH}#${hostname}"

# We don't want "hardware-configuraton.nix" in the git repo
echo "         rm hardware-configuration.nix"
rm hardware-configuration.nix
