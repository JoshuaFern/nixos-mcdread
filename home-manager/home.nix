{ config, pkgs, ... }:

# man home-configuration.nix(5) to list all options
# man page will show e.g: Declared by: <home-manager/modules/programs/mpv.nix>
# https://github.com/nix-community/home-manager/blob/master/modules/programs/mpv.nix

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "mcdread";
  home.homeDirectory = "/home/mcdread";

  home.shellAliases = {
    ls="ls -lh --color=auto";
    lynx="lynx https://lite.duckduckgo.com/lite/";
    rm="rm -I";
  };

  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    enableSyntaxHighlighting = true;
    defaultKeymap = "vicmd";
    initExtra = ''
      autoload -Uz vcs_info
      precmd () { vcs_info }
      zstyle ':vcs_info:' formats '(%b)'
      #$vcs_info_msg_0_
      PS1='%F{blue}[%F{red}%T%F{blue}][%F{green}%1~%F{blue}]%f %F{yellow}%B$ '
    '';
    history = {
      expireDuplicatesFirst = true;
      ignoreDups = true;
    };
    shellAliases = {
      ".." = "cd ..";
      ls="ls -lh --color=auto";
      lynx="lynx https://lite.duckduckgo.com/lite/?q=$1";
      rm="rm -I";
    };
  };

  programs.git = {
    enable = true;
    userName = "mcdread";
    userEmail = "mcdread@protonmail.com";
  };

  wayland.windowManager.sway.config.menu = "PATH=$PATH:${config.xdg.dataHome}/../bin ${pkgs.dmenu-wayland}/bin/dmenu-wl_run";


  home.file.".bashrc".text = ''
    export PS1="\[$(tput bold)\]\[\033[38;5;21m\][\[$(tput sgr0)\]\[\033[38;5;160m\]\A\[$(tput sgr0)\] \[$(tput sgr0)\]\[\033[38;5;2m\]\W\[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;21m\]]\[$(tput sgr0)\] \[$(tput sgr0)\]\[$(tput bold)\]\[\033[38;5;3m\]\\$\[$(tput sgr0)\] \[$(tput sgr0)\]"
    set -o vi
    alias ls="ls -lh --color=auto"
    alias lynx="lynx https://lite.duckduckgo.com/lite";
    alias rm="rm -I";
  '';

  home.file.".xinitrc".text = ''
    exec i3
  '';

  home.file."bin/nixedit.sh" = {
    executable = true;
    text = ''
      #!/bin/sh
      sudo nvim /etc/nixos/configuration.nix
    '';
  };

  home.file."bin/wallpaper_random.sh" = {
      executable = true;
      text = ''
        #!/bin/sh
        while true
        do
	       RND_IMG=`ls ~/images/wallpapers/ | shuf -n 1`
	       eval "feh --bg-max ~/images/wallpapers/$RND_IMG"
	       sleep 240
        done
      '';
  };

  xdg.configFile."mpv/mpv.conf" = {
    text = ''
      script-opts=ytdl_hook-ytdl_path=/run/current-system/sw/bin/yt-dlp
      fullscreen=yes
      demuxer-max-bytes=2GiB
      demuxer-max-back-bytes=2GiB
      volume=40
    '';
  };

  programs.obs-studio.plugins = [ pkgs.obs-studio-plugins.wlrobs ];

  home.file."bin/ytcc_oldest.sh" = {
    executable = true;
    text = ''
      #!/bin/sh
      if [ -z "$2" ] #add any argument to play marked
      then
        ID=$(ytcc -t no list | grep -i "$1" | cut -d ' ' -f 2 | tail -n 1)
      else
        ID=$(ytcc -t no list -w | grep -i "$1" | cut -d ' ' -f 2 | tail -n 1)
      fi

      if [ "$ID" = "" ]
      then
        echo "No id found, playing marked"
        ID=$(ytcc -t no list -w | grep -i "$1" | cut -d ' ' -f 2 | tail -n 1)
      fi

      if [ "$ID" = "" ]
      then
        echo "Sorry, still no id found"
      else
        ytcc play $ID
        echo "played ID:  $ID"
      fi
    '';
  };




  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";
}
